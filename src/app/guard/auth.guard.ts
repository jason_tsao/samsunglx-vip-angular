import { LoginDialogComponent } from './../login-dialog/login-dialog.component';
import { UserLoginService } from './../services/cognito-login.service';
import { AdalService } from './../services/adal.service';
import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, NavigationExtras } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { MatDialog, MatDialogRef } from '@angular/material';

@Injectable()
export class AuthGuard implements CanActivate {
  isLogin = false;

  constructor(
    private router: Router,
    private adalService: AdalService,
    private userService: UserLoginService,
    public dialog: MatDialog
  ) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      // check cognito auth
      this.userService.isAuthenticated(this);
      // check azure AD auth
      if (this.adalService.isAuthenticated) {
        console.log('User is already login through Azure AD!!');
        this.isLogin = true;
      }

      if (this.isLogin) {
        // logged in
        console.log('User is already authorized!!');
      } else {
        // this is call login dialog to login...
        // const dialogRef = this.dialog.open(LoginDialogComponent, {
        //   width: '380px',
        // });
        // dialogRef.afterClosed().subscribe(result => {
        //   if (result === 'success') {
            // redirect to the url user want to go
        //     this.router.navigate([state.url]);
        //   }
        //   this.userService.isAuthenticated(this);
        // });
        // this is direct to loign page to login...
        this.router.navigate(['login']);
        // not logged in so redirect to login page with the return url and return false
        // this.router.navigate(['login'], { queryParams: { returnUrl: state.url }});
      }
      return this.isLogin;
  }
  // cognito callback function
  isLoggedIn(message: string, isLoggedIn: boolean) {
    if (isLoggedIn) {
      console.log('User is already login through cognito!!');
    } else {
      console.log('User is not login through cognito!!');
    }
    this.isLogin = isLoggedIn;
  }
}
