export class TrackingList {
    position: string;
    hawb: string;
    mawb: string;
    // invoice: string;
    type: string;
    pkgs: string;
    wgt: string;
    // departure from Taiwan (export)
    pol_etd: Date;
    // arrive LA (import)
    pod_eta: Date;
    sd_cfs_inbound: Date;
    // arrive SAMEX warehouse (import)
    samex: Date;
    // means this hawb is an emergency (import)
    hot: boolean;
    commodity: string;
    remark: string;
}
