export class PersonInCharge {
    id: number;
    name: string;
    email?: string;
    telephon?: string;
}
