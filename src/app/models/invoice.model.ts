export class Invoice {
    invoice_no: string;
    invoice_date: Date;
    doc_num: string;
    hawb: string;
    invoice_no_list: string[];
}
