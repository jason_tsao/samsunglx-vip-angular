import { Prolink } from './prolink.model';

export class Tracking {
    hawb: string;
    mawb: string;
    invoice: string;
    type: string;
    shipper: string;
    commod: string;
    airline: string;
    flight: string;
    pkgs: string;
    cartons: string;
    cwt: string;
    whs: string;
    // departure from Taiwan (export)
    pol_etd_date: string;
    pol_etd_time: string;
    pol_atd_date: string;
    pol_atd_time: string;
    // arrive LA (import)
    pod_eta_date: string;
    pod_eta_time: string;
    pod_ata_date: string;
    pod_ata_time: string;
    // pickup goods in LA (import)
    lax_pickup_date: string;
    lax_pickup_time: string;
    // deliver to LA Container Freight Station (import)
    la_cfs_delivery_date: string;
    la_cfs_delivery_time: string;
    // deliver to SAN Container Freight Station (import)
    sd_cfs_inbound_date: string;
    sd_cfs_inbound_time: string;
    // departure from SAN Container Freight Station (import)
    sd_cfs_outbound_date: string;
    sd_cfs_outbound_time: string;
    // arrive SAMEX warehouse (import)
    samex_date: string;
    samex_time: string;
    eta_samex_req_date: Date;
    // means this hawb is an emergency (import)
    hot: boolean;
    // recieved inbound delivery EDI message
    idr_received: boolean;
    // samsung incharge person
    incharge: number;
    remark: string;

    public setProlinkData(data: Prolink) {
        this.mawb = data.mawb;
        this.invoice = data.invoice;
        this.commod = data.commod;
        this.shipper = data.shipper;
        this.flight = data.flight;
        this.pkgs = data.pkgs;
        this.cartons = data.cartons;
        this.cwt = data.cwt;
    }
}
