export class Prolink {
    hawb: string;
    mawb: string;
    invoice: string;
    commod: string;
    shipper: string;
    flight: string;
    pkgs: string;
    cartons: string;
    cwt: string;
    etd_pol: Date;
    atd_pol: Date;
    eta_pod: Date;
    ata_pod: Date;
}
