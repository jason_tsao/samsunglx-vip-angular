/**
 * ref: https://codecraft.tv/courses/angular/unit-testing/overview/
 *      https://ordina-jworks.github.io/angular/2017/10/04/Testing-angular-with-karma.html#what-to-test
 *
 */
import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
// for material module test
import { MaterialModule } from './material.module';
// for provide the service we use
import { LoginConfigService } from './services/login-config.service';
import { AdalService } from './services/adal.service';
import { AuthGuard } from './guard/auth.guard';
import { UserLoginService } from './services/cognito-login.service';
import { UserRegistrationService } from './services/user-registration.service';
import { AwsUtil } from './services/aws.service';
import { LocalStorageHelper } from './util/localStorage';
import { CognitoUtil } from './services/cognito.service';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
          RouterTestingModule,
          MaterialModule,
      ],
      providers: [
        LoginConfigService,
        AdalService,
        AuthGuard,
        UserLoginService,
        CognitoUtil,
        AwsUtil,
        LocalStorageHelper,
        UserRegistrationService,
      ],
      schemas: [
        NO_ERRORS_SCHEMA,
      ],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'app'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('app');
  }));
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to app!');
  }));
});
