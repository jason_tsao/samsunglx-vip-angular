import { MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';
import { DataService } from '../services/data.service';
import { Invoice } from '../models/Invoice.model';

@Component({
  selector: 'app-invoice-pickup-dialog',
  templateUrl: './invoice-pickup-dialog.component.html',
  styleUrls: ['./invoice-pickup-dialog.component.css']
})
export class InvoicePickupDialogComponent implements OnInit {
  hawb: string;
  invoice_no_list=[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: object,
    private dataservice: DataService
  ) {
    console.log(JSON.stringify(data));
  }

  ngOnInit() {
  }

  updateInvoic(){
    this.data["items"].forEach(element => {
      this.invoice_no_list.push(element["invoice_no"]);
    });
    console.log(this.invoice_no_list);
    this.dataservice.postUpdateInvoic(this.hawb, this.invoice_no_list).subscribe(result=>{
    });;
  }
}
