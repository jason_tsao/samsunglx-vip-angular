import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoicePickupDialogComponent } from './invoice-pickup-dialog.component';

describe('InvoicePickupDialogComponent', () => {
  let component: InvoicePickupDialogComponent;
  let fixture: ComponentFixture<InvoicePickupDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoicePickupDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoicePickupDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
