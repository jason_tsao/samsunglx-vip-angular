import { DateUtil } from './../util/dateUtil';
import { FormGroup, Validators, FormControl, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource, PageEvent, MatPaginator } from '@angular/material';
import saveAs from 'save-as';
// declare var FileSaver;
// import * as _ from 'FileSaver';
// in .angular-cli.json add scripts { 'input': './assets/FileSaver.js', 'output': 'FileSaver' },
export class SearchModel {
  public hawb: string;
  public startDate: Date;
  public endDate: Date;

  constructor(hawb?: string, startDate?: Date, endDate?: Date) {
    this.hawb = hawb;
    this.startDate = startDate;
    this.endDate = endDate;
  }
}

@Component({
  selector: 'app-check-list-report',
  templateUrl: './check-list-report.component.html',
  styleUrls: ['./check-list-report.component.css']
})
export class CheckListReportComponent implements OnInit, AfterViewInit {
  element_data = ELEMENT_DATA;
  displayedColumns = ['position', 'name', 'weight', 'symbol'];
  dataSource: MatTableDataSource<Element>;
  searchModel: SearchModel;
  serachForm: FormGroup;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  // MatTable Inputs

  constructor(public router: Router) {
    this.dataSource = new MatTableDataSource(this.element_data);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit() {
    this.searchModel = new SearchModel();
    // model driven form
    this.serachForm = new FormGroup({
      'hawb': new FormControl(this.searchModel.hawb),
      'startDate': new FormControl(this.searchModel.startDate),
      'endDate': new FormControl(this.searchModel.endDate),
    });
  }

  printExcel() {
    const blob = new Blob([document.getElementById('exportable').innerHTML], {
      type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8'
    });
    saveAs(blob, 'DailyCheckList_SamsungTIJ_' + new Date().toISOString().replace(/[\-\:\.]/g, '') + '.xls');

  }

  search() {
    alert('you are searching hawb:' + this.searchModel.hawb + ', date between:' +
      DateUtil.getPattern(this.searchModel.startDate, 'yyyyMMdd') + '~' + DateUtil.getPattern(this.searchModel.endDate, 'yyyyMMdd'));
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
}

export class Element {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: Element[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
  {position: 11, name: 'Sodium', weight: 22.9897, symbol: 'Na'},
  {position: 12, name: 'Magnesium', weight: 24.305, symbol: 'Mg'},
  {position: 13, name: 'Aluminum', weight: 26.9815, symbol: 'Al'},
  {position: 14, name: 'Silicon', weight: 28.0855, symbol: 'Si'},
  {position: 15, name: 'Phosphorus', weight: 30.9738, symbol: 'P'},
  {position: 16, name: 'Sulfur', weight: 32.065, symbol: 'S'},
  {position: 17, name: 'Chlorine', weight: 35.453, symbol: 'Cl'},
  {position: 18, name: 'Argon', weight: 39.948, symbol: 'Ar'},
  {position: 19, name: 'Potassium', weight: 39.0983, symbol: 'K'},
  {position: 20, name: 'Calcium', weight: 40.078, symbol: 'Ca'},
];

