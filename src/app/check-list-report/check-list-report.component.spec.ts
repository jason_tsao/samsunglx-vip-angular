import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { By } from '@angular/platform-browser';
import { CheckListReportComponent } from './check-list-report.component';

describe('CheckListReportComponent', () => {
  let component: CheckListReportComponent;
  let fixture: ComponentFixture<CheckListReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckListReportComponent ]
    })
    .compileComponents();
  }));

  // This function is called before each test specification, it function, has been run.
  beforeEach(() => {
    fixture = TestBed.createComponent(CheckListReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // Test case start
  it('should create', () => {
    // expected result
    expect(component).toBeTruthy();
  });

  // it('should have expected <h1> text', () => {
  //   // arrange
  //   const expectedPattern = /^This is Angular \d\.\d\.\d$/g;
  //   const message = '<h1>should display \"This is Angular\" and version number.';

  //   // act
  //   fixture.detectChanges();
  //   const actual = fixture.debugElement.query(By.css('h1')).nativeElement.innerText;

  //   // assert
  //   expect(actual).toMatch(expectedPattern, message);
  // });
});
