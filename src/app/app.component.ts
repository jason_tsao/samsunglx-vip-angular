import { LocalStorageHelper } from './util/localStorage';
import { LoginDialogComponent } from './login-dialog/login-dialog.component';
import { UserLoginService } from './services/cognito-login.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AdalService } from './services/adal.service';
import { Component, OnInit } from '@angular/core';
import { environment } from '../environments/environment';
import { MatSidenav, MatDrawerToggleResult, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Callback, CognitoUtil, LoggedInCallback } from './services/cognito.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Samsung LAX App';
  isHideMenu = true;
  env: string;
  isProduction: boolean;
  apiUrl: string;
  returnUrl: string;

  ngOnInit() {
    console.log('AppComponent ngOnInit...');
    // if redirect from adal ===> should excute adalService.handleCallback
    this.adalService.handleCallback();

    if (this.adalService.isAuthenticated) {
      console.log('Azure AD logged in');

      localStorage.setItem('adalAccessToken', this.adalService.accessToken);
      localStorage.setItem('isAuthenticated', this.adalService.isAuthenticated);
      localStorage.setItem('userInfo', JSON.stringify(this.adalService.userInfo));
      localStorage.setItem('userName', this.adalService.userInfo.userName);
      // get return url from route parameters or default to '/'
      // this.returnUrl = this.route.snapshot.queryParams['returnUrl'];
      // redirect to return url
      // console.log('returnUrl=' + this.returnUrl);
      // if (this.returnUrl) {
        // this.router.navigateByUrl(this.returnUrl);
      // }
      this.router.navigateByUrl('/secure');
    }
  }

  toggleSideNav(sideNav: MatSidenav) {
    sideNav.toggle().then((result: any) => {
      console.log(result);
      console.log(`選單狀態：${result.type}`);
    });
  }

  opened() {
    console.log('芝麻開門');
  }

  closed() {
    console.log('芝麻關門');
  }

  constructor(
      public adalService: AdalService,
      public userService: UserLoginService,
      private router: Router,
      private route: ActivatedRoute,
      public cognitoUtil: CognitoUtil,
      public dialog: MatDialog,
      public helper: LocalStorageHelper
    ) {
      this.env = environment.env;
      this.isProduction = environment.production;
      this.apiUrl = environment.dataServiceUrl;
  }

  onLogout() {
    console.log('onLogout..........');
    if (this.adalService.isAuthenticated) {
      this.adalService.logout();
    } else {
      this.userService.logout();
    }
    localStorage.clear();
    this.router.navigate(['/']);
  }

  loginDialog() {
    const dialogRef = this.dialog.open(LoginDialogComponent, {
      width: '380px',
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed with result = ' + result);
    });
  }
}