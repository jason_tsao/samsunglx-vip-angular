import { LocalStorageHelper } from '../util/localStorage';
import { LoginDialogComponent } from '../login-dialog/login-dialog.component';
import { UserLoginService } from '../services/cognito-login.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AdalService } from '../services/adal.service';
import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import { MatSidenav, MatDrawerToggleResult, MatDialog, MatDialogRef } from '@angular/material';
import { Callback, CognitoUtil, LoggedInCallback } from '../services/cognito.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  isHideMenu = true;
  returnUrl: string;

  ngOnInit() {
    console.log('AppComponent ngOnInit...');
    // if redirect from adal ===> should excute adalService.handleCallback
    this.adalService.handleCallback();

    if (this.adalService.isAuthenticated) {
      console.log('Azure AD logged in');

      localStorage.setItem('adalAccessToken', this.adalService.accessToken);
      localStorage.setItem('isAuthenticated', this.adalService.isAuthenticated);
      localStorage.setItem('userInfo', JSON.stringify(this.adalService.userInfo));
      localStorage.setItem('userName', this.adalService.userInfo.userName);

    }
  }

  toggleSideNav(sideNav: MatSidenav) {
    sideNav.toggle().then((result: any) => {
      console.log(result);
      console.log(`選單狀態：${result.type}`);
    });
  }

  opened() {
    console.log('芝麻開門');
  }

  closed() {
    console.log('芝麻關門');
  }

  constructor(
      public adalService: AdalService,
      public userService: UserLoginService,
      private router: Router,
      private route: ActivatedRoute,
      public cognitoUtil: CognitoUtil,
      public dialog: MatDialog,
      public helper: LocalStorageHelper
    ) {
  }
}
