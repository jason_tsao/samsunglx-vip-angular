import { MatDialog, MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import { UserLoginService } from './../services/cognito-login.service';
import { AdalService } from './../services/adal.service';
import { CognitoUtil, Callback } from './../services/cognito.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';

export class LoginModel {
  email: string;
  password: string;
  accessToken: string;
  isAuthenticated: string;
  userInfo: adal.User;
}

@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.css']
})
export class LoginDialogComponent implements OnInit {
  loginForm: FormGroup;
  loginModel: LoginModel;
  errorMessage: string;

  constructor(
    public router: Router,
    public adalService: AdalService,
    public userService: UserLoginService,
    public matDialog: MatDialog,
    public cognitoUtil: CognitoUtil,
    public dialogRef: MatDialogRef<LoginDialogComponent>
  ) {
    this.loginModel = new LoginModel();
    this.loginForm = new FormGroup({
      'email': new FormControl(''),
      'password': new FormControl(''),
    });
  }

  ngOnInit() {
  }

  cognitoLogin() {
    if (this.loginModel.email == null || this.loginModel.password == null) {
        this.errorMessage = 'All fields are required';
        return;
    }
    this.errorMessage = null;
    this.userService.authenticate(this.loginModel.email, this.loginModel.password, this);
  }

  cancel() {
  }

  adalLogin() {
    if (!this.adalService.isAuthenticated) {
      this.adalService.login();
    }
  }

  // cognito login callback function
  cognitoCallback(message: string, result: any) {
    if (message != null) { // error
        this.errorMessage = message;
        console.log('result: ' + this.errorMessage);
        // this.dialogRef.close('error');
        return false;
        // if (this.errorMessage === 'User is not confirmed.') {
        //     console.log('redirecting');
        //     this.router.navigate(['/secure/confirmRegistration', this.loginModel.email]);
        // } else if (this.errorMessage === 'User needs to set password.') {
        //     console.log('redirecting to set new password');
        //     this.router.navigate(['/secure/newPassword']);
        // }
    } else { // success
        console.log('login success!!!');
        // this.ddb.writeLogEntry('login');
        localStorage.setItem('userName', this.loginModel.email);
        this.cognitoUtil.getAccessToken(new AccessTokenCallback(this));
        this.cognitoUtil.getIdToken(new IdTokenCallback(this));
        this.dialogRef.close('success');
        // this.router.navigate(['/secure']);
        return true;
    }
  }
}

export class AccessTokenCallback implements Callback {
  constructor(public jwt: LoginDialogComponent) {
  }
  callback() {
  }
  callbackWithParam(result) {
    localStorage.setItem('awsAccessToken', result);
  }
}

export class IdTokenCallback implements Callback {
  constructor(public jwt: LoginDialogComponent) {
  }
  callback() {
  }
  callbackWithParam(result) {
    localStorage.setItem('awsIdToken', result);
  }
}
