import { Directive, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators } from '@angular/forms';

/** A field value can't match the given regular expression */
export function dateValidator(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {
    const result =  control.value && !(control.value instanceof Date);   // check is empty or is Date object
    return result ? {'dateCheck': {value: control.value}} : null;
  };
}
