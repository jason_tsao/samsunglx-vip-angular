import { Directive, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators } from '@angular/forms';

/* custom validator sample */
/** A field value can't match the given regular expression */
export function regValidator(pattern: string): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {
    console.log('control.value = ' + control.value);
    const result = new RegExp(pattern).test(control.value) || !control.value; // check is empty or fit the pattern
    return !result ? {'regCheck': {value: control.value}} : null;
  };
}
