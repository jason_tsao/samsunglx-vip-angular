import { UserLoginService } from './../../services/cognito-login.service';
import { AdalService } from './../../services/adal.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private router: Router, public adalService: AdalService, public userService: UserLoginService) {
  }

  ngOnInit() {
    if (this.adalService.isAuthenticated) {
      console.log('into LogoutComponent ngOnInit... adal logout');
      this.adalService.logout();
    } else {
      console.log('into LogoutComponent ngOnInit... cognito logout');
      this.userService.logout();
    }
    // hack for error: Expression has changed after it was checked
    setTimeout(() =>
      {
        localStorage.setItem('userName', '');
        localStorage.setItem('awsIdToken', '');
        localStorage.setItem('awsAccessToken', '');
        localStorage.setItem('adalAccessToken', '');
        localStorage.setItem('userInfo', '');
      } , 0);
    // localStorage.clear();
    // localStorage.setItem('userName', '');
    this.router.navigate(['/']);
  }

}
