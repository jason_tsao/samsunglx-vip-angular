import { UserLoginService } from './../../services/cognito-login.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-forgot-password-step2',
  templateUrl: './forgot-password-step2.component.html',
  styleUrls: ['./forgot-password-step2.component.css', '../shared.css']
})
export class ForgotPasswordStep2Component implements OnInit, OnDestroy {
  email: string;
  password: string;
  errorMessage: string;
  verificationCode: string;
  private sub: any;

  constructor(public router: Router, public userService: UserLoginService, public route: ActivatedRoute) {
    this.errorMessage = null;
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.email = params['username'];

    });
  }
  ngOnDestroy() {
      this.sub.unsubscribe();
  }

  nextStep() {
      this.errorMessage = null;
      this.userService.confirmNewPassword(this.email, this.verificationCode, this.password, this);
  }

  cognitoCallback(message: string) {
      if (message != null) { // error
          this.errorMessage = message;
      } else { // success
          this.router.navigate(['/login']);
      }
  }
}
