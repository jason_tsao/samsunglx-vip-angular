<div id="app" class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
          <h1>Forgot/Change Password</h1>
      </div>
      <div class="panel-body mat-elevation-z4">
          <div class="container">
              <!-- <form class="example-container"  [formGroup]="resendForm"> -->
                <div class="row">
                  <div class="col-md-4">
                    <label>EMAIL: </label>
                    <mat-form-field style="width: 300px">
                      <input matInput placeholder="" [(ngModel)]="email">
                    </mat-form-field>
                  </div>
                </div>
                <div class="row">
                  <mat-error *ngIf="errorMessage">{{errorMessage}}</mat-error>
                </div>
              <!-- </form> -->
              <div class="row">
                <div class="col-md-6">
                    <button mat-raised-button color="primary" (click)="nextStep()">Reset Paswword</button>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                   Suddenly remembered your password?<a routerLink="/login"> Login </a>
                </div>
              </div>
          </div>
      </div>
  </div>
  