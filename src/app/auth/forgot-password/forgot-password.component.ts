import { UserLoginService } from './../../services/cognito-login.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css', '../shared.css']
})
export class ForgotPasswordComponent implements OnInit {
  email: string;
  errorMessage: string;

  constructor(public router: Router,
              public userService: UserLoginService) {
      this.errorMessage = null;
  }

  ngOnInit() {
  }

  nextStep() {
    console.log('onNext()..........');
    this.errorMessage = null;
    this.userService.forgotPassword(this.email, this);
  }

  cognitoCallback(message: string, result: any) {
    if (message == null && result == null) { // success
        this.router.navigate(['/login/forgotPassword', this.email]);
    } else {  // error
        this.errorMessage = message;
    }
  }
}
