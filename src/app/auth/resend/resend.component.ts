import { Router, ActivatedRoute } from '@angular/router';
import { UserRegistrationService } from './../../services/user-registration.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-resend',
  templateUrl: './resend.component.html',
  styleUrls: ['./resend.component.css', '../shared.css']
})
export class ResendComponent implements OnInit {
  email: string;
  errorMessage: string;
  private sub: any;

  constructor(public userRegistrationService: UserRegistrationService, public router: Router, public route: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.email = params['username'];
    });
  }

  resendCode() {
    this.userRegistrationService.resendCode(this.email, this);
  }

  cognitoCallback(error: any, result: any){
    if (error != null){
      this.errorMessage = 'Something went worng... please try again';
    } else {
      this.router.navigate(['/login/confirm', this.email]);
    }
  }

}
