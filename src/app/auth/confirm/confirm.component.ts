import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UserRegistrationService } from './../../services/user-registration.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

export class ConfirmationModel {
  confirmationCode: string;
  email: string;
}

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.css', '../shared.css']
})
export class ConfirmComponent implements OnInit, OnDestroy {
  errorMessage: string;
  private sub: any;
  confirmationModel: ConfirmationModel;
  confirmationForm: FormGroup;

  constructor(public userRegistrationService: UserRegistrationService, public router: Router, public route: ActivatedRoute) {
    this.confirmationModel = new ConfirmationModel();
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.confirmationModel.email = params['username'];
    });
    this.errorMessage = null;
    this.confirmationForm = new FormGroup({
      'email': new FormControl(this.confirmationModel.email),
      'confirmationCode': new FormControl(this.confirmationModel.confirmationCode),
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  onConfirmRegistration() {
    this.errorMessage = null;
    this.userRegistrationService.confirmRegistration(this.confirmationModel.email, this.confirmationModel.confirmationCode, this);
  }

  cognitoCallback(message: string, result: any) {
    if (message != null) { // error
        this.errorMessage = message;
    } else { // success
        // move to the next step
        this.router.navigate(['/login']);
    }
  }
}
