<div id="app" class="container-fluid">
  <div class="panel panel-default">
    <div class="panel-heading">
        <h1>Login</h1>
    </div>
    <div class="panel-body mat-elevation-z4">
        <div class="container">
            <form class="example-container" [formGroup]="loginForm">
              <div class="row">
                <div class="col-md-4">
                  <label>EMAIL: </label>
                  <mat-form-field style="width: 300px">
                    <input matInput placeholder="" formControlName="email" [(ngModel)]="loginModel.email">
                  </mat-form-field>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <label>PASSWORD: </label>
                  <mat-form-field style="width: 265px">
                    <input matInput placeholder="" formControlName="password" [(ngModel)]="loginModel.password">
                  </mat-form-field>
                </div>
              </div>
              <div class="row">
                <mat-error *ngIf="errorMessage">{{errorMessage}}</mat-error>
              </div>
            </form>
            <div class="row">
              <div class="col-md-1">
                  <button mat-raised-button color="primary" (click)="cognitoLogin()">Login</button>
              </div>
              <div class="col-md-6">
                 <button mat-raised-button color="warn" (click)="adalLogin()" style="float:left">Morrison Login</button>
              </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                  <a routerLink="/login/register">Register</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                  <a routerLink="/login/resendCode">Resend Verification Code</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                  <a routerLink="/login/confirm">Confirm your account</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                  <a routerLink="/login/forgotPassword">Forgot/Change Password?</a>
                </div>
            </div>
            <!-- <div class="row">
                <div class="col-md-6">
                  <a routerLink="/login/newPassword">Change Password</a>
                </div>
            </div> -->
            <div class="row">
                <div class="col-md-6">
                   <mat-hint> Hint : Morrison employee please click the "Morrison Login" button to another page to login.</mat-hint>
                </div>
            </div>
        </div>
    </div>
</div>
