import { CognitoUtil, Callback } from './../../services/cognito.service';
import { UserLoginService } from './../../services/cognito-login.service';
import { AdalService } from './../../services/adal.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

export class LoginModel {
  email: string;
  password: string;
  accessToken: string;
  isAuthenticated: string;
  userInfo: adal.User;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css', '../shared.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loginModel: LoginModel;
  errorMessage: string;
  returnUrl: string;

  constructor(
    public router: Router,
    public adalService: AdalService,
    public userService: UserLoginService,
    private activatedRoute: ActivatedRoute,
    public cognitoUtil: CognitoUtil
  ) {
    this.loginModel = new LoginModel();
    this.loginForm = new FormGroup({
      'email': new FormControl(''),
      'password': new FormControl(''),
    });
  }

  ngOnInit() {
    // subscribe to router event
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.returnUrl = params['returnUrl'];
      console.log(this.returnUrl);
    });
  }

  cognitoLogin() {
    if (this.loginModel.email == null || this.loginModel.password == null) {
        this.errorMessage = 'All fields are required';
        return;
    }
    this.errorMessage = null;
    this.userService.authenticate(this.loginModel.email, this.loginModel.password, this);
  }

  cancel() {
  }

  adalLogin() {
    if (!this.adalService.isAuthenticated) {
      this.adalService.login();
    }
  }

  // cognito login callback function
  cognitoCallback(message: string, result: any) {
    if (message != null) { // error
        this.errorMessage = message;
        console.log('result: ' + this.errorMessage);
        // if (this.errorMessage === 'User is not confirmed.') {
        //     console.log('redirecting');
        //     this.router.navigate(['/secure/confirmRegistration', this.loginModel.email]);
        // } else if (this.errorMessage === 'User needs to set password.') {
        //     console.log('redirecting to set new password');
        //     this.router.navigate(['/secure/newPassword']);
        // }
    } else { // success
        console.log('success redirecting to Home');
        localStorage.setItem('userName', this.loginModel.email);
        this.cognitoUtil.getAccessToken(new AccessTokenCallback(this));
        this.cognitoUtil.getIdToken(new IdTokenCallback(this));
        // this.ddb.writeLogEntry('login');
        this.router.navigate([this.returnUrl ? this.returnUrl : '/secure/dashboard']);
    }
  }
}

export class AccessTokenCallback implements Callback {
  constructor(public jwt: LoginComponent) {
  }
  callback() {
  }
  callbackWithParam(result) {
    localStorage.setItem('awsAccessToken', result);
  }
}

export class IdTokenCallback implements Callback {
  constructor(public jwt: LoginComponent) {
  }
  callback() {
  }
  callbackWithParam(result) {
    localStorage.setItem('awsIdToken', result);
  }
}
