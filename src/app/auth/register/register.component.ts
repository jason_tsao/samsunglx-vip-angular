import { UserRegistrationService } from './../../services/user-registration.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

export class RegistrationUser {
  name: string;
  email: string;
  password: string;
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css', '../shared.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  registrationUser: RegistrationUser;
  router: Router;
  errorMessage: String;

  constructor(public userRegistration: UserRegistrationService, router: Router) {
    this.router = router;
    this.registrationUser = new RegistrationUser();
    this.registerForm = new FormGroup({
      'name': new FormControl(''),
      'email': new FormControl(''),
      'password': new FormControl(''),
    });
  }

  ngOnInit() {
  }

  onRegister() {
    if (this.registrationUser.email && this.registrationUser.name && this.registrationUser.password) {
      this.errorMessage = null;
      this.userRegistration.register(this.registrationUser, this);
    } else {
      this.errorMessage = 'Must fill all fields.';
    }
  }

  cognitoCallback(message: string, result: any) {
    if (message != null) { // error
        this.errorMessage = message;
        console.log('result: ' + this.errorMessage);
    } else { // success
        // move to the next step
        this.router.navigate(['/login/confirm', result.user.username]);
    }
  }
}
