<div id="app" class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
          <h1>Register</h1>
      </div>
      <div class="panel-body mat-elevation-z4">
          <div class="container">
              <form class="example-container" [formGroup]="registerForm">
                <div class="row">
                  <div class="col-md-4">
                    <label>NAME: </label>
                    <mat-form-field style="width: 300px">
                      <input matInput placeholder="" formControlName="name" [(ngModel)]="registrationUser.name">
                    </mat-form-field>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <label>EMAIL: </label>
                    <mat-form-field style="width: 300px">
                      <input matInput placeholder="" formControlName="email" [(ngModel)]="registrationUser.email">
                    </mat-form-field>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <label>PASSWORD: </label>
                    <mat-form-field style="width: 265px">
                      <input matInput placeholder="" formControlName="password" [(ngModel)]="registrationUser.password">
                    </mat-form-field>
                  </div>
                </div>
                <div class="row">
                  <mat-error *ngIf="errorMessage">{{errorMessage}}</mat-error>
                </div>
              </form>
              <div class="row">
                <div class="col-md-6">
                    <button mat-raised-button color="primary" (click)="onRegister()">Create Account</button>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                   Already have an account?<a routerLink="/login"> Login </a>
                </div>
              </div>
          </div>
      </div>
  </div>
  