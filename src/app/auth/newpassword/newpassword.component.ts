import { UserLoginService } from './../../services/cognito-login.service';
import { UserRegistrationService } from './../../services/user-registration.service';
import { CognitoUtil, Callback } from './../../services/cognito.service';
import { CognitoCallback } from './../../services/cognito.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

export class NewPasswordUser {
  username: string;
  existingPassword: string;
  password: string;
}
// this is not function... because of the this.userRegistrationService.newPassword(this.newPasswordUser, this);
@Component({
  selector: 'app-newpassword',
  templateUrl: './newpassword.component.html',
  styleUrls: ['./newpassword.component.css']
})
export class NewpasswordComponent implements OnInit, CognitoCallback {
  newPasswordUser: NewPasswordUser;
  router: Router;
  errorMessage: String;

  constructor(
    public userRegistrationService: UserRegistrationService,
    public userLoginService: UserLoginService,
    router: Router,
    public cognitoUtil: CognitoUtil
  ) {
    this.router = router;
  }

  ngOnInit() {
    this.newPasswordUser = new NewPasswordUser();
    this.errorMessage = null;
    console.log('Checking if the user is already authenticated. If so, then redirect to the secure site');
    this.userLoginService.isAuthenticated(this);
  }

  onNewPassword() {
    console.log(this.newPasswordUser);
    this.errorMessage = null;
    this.userRegistrationService.newPassword(this.newPasswordUser, this);
  }

  cognitoCallback(message: string, result: any) {
    alert(message);
    if (message != null) { // error
        this.errorMessage = message;
        console.log('result: ' + this.errorMessage);
    } else { // success
        // move to the next step
        localStorage.setItem('userName', this.newPasswordUser.username);
        this.cognitoUtil.getAccessToken(new AccessTokenCallback(this));
        this.cognitoUtil.getIdToken(new IdTokenCallback(this));
        this.router.navigate(['/']);
    }
  }

  isLoggedIn(message: string, isLoggedIn: boolean) {
    console.log('NewpasswordComponent...' + isLoggedIn);
    if (isLoggedIn) {
        this.router.navigate(['/']);
      }
  }
}


export class AccessTokenCallback implements Callback {
  constructor(public jwt: NewpasswordComponent) {
  }
  callback() {
  }
  callbackWithParam(result) {
    localStorage.setItem('awsAccessToken', result);
  }
}

export class IdTokenCallback implements Callback {
  constructor(public jwt: NewpasswordComponent) {
  }
  callback() {
  }
  callbackWithParam(result) {
    localStorage.setItem('awsIdToken', result);
  }
}
