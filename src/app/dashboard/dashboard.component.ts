import { DateUtil } from './../util/dateUtil';
import { FormGroup, Validators, FormControl, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource, PageEvent, MatPaginator, MatDialog, MatDialogRef, MatSnackBar, MatSort, Sort } from '@angular/material';
import { DataService } from '../services/data.service';
import { TrackingList } from "../models/TrackingList.model";

export class SearchModel {
  public hawb: string;
  public startDate: Date;
  public endDate: Date;

  constructor(hawb?: string, startDate?: Date, endDate?: Date) {
    this.hawb = hawb;
    this.startDate = startDate;
    this.endDate = endDate;
  }
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, AfterViewInit {
  displayedColumns = ['position', 'mawb', 'hawb', 'type', 'pkgs', 'wgt', 'pol_etd', 'pod_eta', 'sd_cfs_inbound', 'hot', 'commodity', 'remark'];
  dataSource = new MatTableDataSource<TrackingList>();
  searchModel: SearchModel;
  serachForm: FormGroup;
  @ViewChild('sortTable') sortTable: MatSort;

  @ViewChild('paginator') paginator: MatPaginator;
  currentPage: PageEvent;
  currentSort: Sort;
  totalCount: number;
  // MatTable Inputs

  constructor(public router: Router,
    private dataservice: DataService,
    private snackBar: MatSnackBar) {
  }

  ngAfterViewInit() {
  }

  ngOnInit() {
    this.currentPage = {
      pageIndex: 0,
      pageSize: 10,
      length: null
    };
    this.currentSort = {
      active: '',
      direction: ''
    };
    this.searchModel = new SearchModel('', new Date(), new Date());
    // model driven form
    this.serachForm = new FormGroup({
      'hawb': new FormControl(this.searchModel.hawb),
      'startDate': new FormControl(this.searchModel.startDate),
      'endDate': new FormControl(this.searchModel.endDate),
    });

    this.getTrackingList();
    
    // 分頁切換時，重新取得資料
    this.paginator.page.subscribe((page: PageEvent) => {
      this.currentPage = page;
      this.getTrackingList();
    });
  }

  // applyFilter(filterValue: string) {
  //   filterValue = filterValue.trim(); // Remove whitespace
  //   filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
  //   this.dataSource.filter = filterValue;
  // }

  getTrackingList() {
    this.dataservice.getTrackingList(
      this.searchModel.startDate.getTime(),
      this.searchModel.endDate.getTime(),
      this.currentPage.pageIndex,
      this.currentPage.pageSize
    ).subscribe(data=>{
      this.totalCount = data.total_count;
      this.dataSource.data = data.trackingList;
      this.dataSource.paginator = this.paginator;
    });
  }
}
