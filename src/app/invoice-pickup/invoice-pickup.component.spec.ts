import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoicePickupComponent } from './invoice-pickup.component';

describe('InvoicePickupComponent', () => {
  let component: InvoicePickupComponent;
  let fixture: ComponentFixture<InvoicePickupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoicePickupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoicePickupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
