import { InvoicePickupDialogComponent } from './../invoice-pickup-dialog/invoice-pickup-dialog.component';
import { DateUtil } from './../util/dateUtil';
import { FormGroup, Validators, FormControl, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource, PageEvent, MatPaginator, MatDialog, MatDialogRef, MatSnackBar, MatSort, Sort } from '@angular/material';
import { DataService } from '../services/data.service';
import { Invoice } from '../models/Invoice.model';

export class SearchModel {
  public hawb: string;
  public startDate: Date;
  public endDate: Date;

  constructor(hawb?: string, startDate?: Date, endDate?: Date) {
    this.hawb = hawb;
    this.startDate = startDate;
    this.endDate = endDate;
  }
}

@Component({
  selector: 'app-invoice-pickup',
  templateUrl: './invoice-pickup.component.html',
  styleUrls: ['./invoice-pickup.component.css']
})
export class InvoicePickupComponent implements OnInit, AfterViewInit {
  displayedColumns = ['checked', 'invoice_no', 'doc_num', 'invoice_date'];
  dataSource = new MatTableDataSource<any>();
  selection = new SelectionModel<Invoice>(true, []);
  searchModel: SearchModel;
  serachForm: FormGroup;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  currentPage: PageEvent;
  currentSort: Sort;
  // MatTable Inputs

  constructor(
    public router: Router,
    public matDialog: MatDialog,
    private snackBar: MatSnackBar,
    private dataservice: DataService
  ) {}

  ngAfterViewInit() {
  }

  ngOnInit() {
    this.currentPage = {
      pageIndex: 0,
      pageSize: 10,
      length: null
    };
    this.currentSort = {
      active: '',
      direction: ''
    };
    this.searchModel = new SearchModel('', new Date(), new Date());
    // model driven form
    this.serachForm = new FormGroup({
      'hawb': new FormControl(this.searchModel.hawb),
      'startDate': new FormControl(this.searchModel.startDate),
      'endDate': new FormControl(this.searchModel.endDate),
    });
    // 分頁切換時，重新取得資料
    this.paginator.page.subscribe((page: PageEvent) => {
      this.currentPage = page;
      this.getInvoicList();
    });

  }

  select() {
    const selectedItems: Invoice[] = [];
    this.dataSource.data.forEach(element => {
      if (this.selection.isSelected(element)) {
        selectedItems.push(element);
      }
    });
    if (selectedItems.length == 0) {
      this.snackBar.open('Please select invoice to combine!', 'Okay');
    } else {// alert(JSON.stringify(selectedItems));
      this.snackBar.dismiss();
      const matDialogRef = this.matDialog.open(InvoicePickupDialogComponent, {
        // inject data to dialog
        data: {
          items: selectedItems,
        }
      });
      matDialogRef.afterClosed().subscribe(result => {
        // here to bulk update the data and do something you want
        if (result === true) {
          this.snackBar.open('bulk update procceed', 'Okay');
          // remove the select items from data
          this.dataSource.data = this.dataSource.data.filter(function(element) {
                // return element.position !== 1;
                let count = 0;
                selectedItems.forEach(item => {
                  if (element.invoice_no === item.invoice_no) {
                    count += 1;
                  }
                });
                return count === 0 ;
          });
          // refresh table
          this.dataSource = new MatTableDataSource(this.dataSource.data);
          // refresh table's paginator
          this.dataSource.paginator = this.paginator;
        } else {
          this.snackBar.open('bulk update cancel', 'Okay');
        }
      });
    }
  }

  search() {
    if (this.searchModel.startDate && this.searchModel.endDate) {
      this.snackBar.dismiss();
      this.getInvoicList();
    } else {
      this.snackBar.open('Please keyin the date range', 'Okay');
    }
    // alert('you are searching hawb:' + this.searchModel.hawb + ', date between:' +
    //   DateUtil.getPattern(this.searchModel.startDate, 'yyyymmdd') + '~' + DateUtil.getPattern(this.searchModel.endDate, 'yyyymmdd'));
    console.log(this.searchModel.startDate.toTimeString());
    console.log(this.searchModel.startDate.toLocaleDateString());
    console.log(this.searchModel.startDate.toLocaleTimeString());
    console.log(this.searchModel.startDate.toLocaleString());

    console.log(this.searchModel.endDate.getTime());
    // console.log(this.currentPage.pageIndex);
    // console.log(this.currentPage.pageSize);
    
  }

  // applyFilter(filterValue: string) {
  //   filterValue = filterValue.trim(); // Remove whitespace
  //   filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
  //   this.dataSource.filter = filterValue;
  // }

  getInvoicList(){
    this.dataservice.getInvoicList(
      this.searchModel.startDate.getTime(),
      this.searchModel.endDate.getTime(),
      this.currentPage.pageIndex,
      this.currentPage.pageSize
    ).subscribe(data=>{
      this.dataSource.data = data.invoicList;
      this.dataSource.paginator = this.paginator;
    });
  }
}