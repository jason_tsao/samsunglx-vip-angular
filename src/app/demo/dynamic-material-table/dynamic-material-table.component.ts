import { MatTableDataSource, PageEvent, MatPaginator, MatSort, Sort, MatPaginatorIntl } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';

/**
 * backend sorting
 * https://github.com/angular/material2/issues/5670
 * https://ithelp.ithome.com.tw/articles/10196827
 */
@Component({
  selector: 'app-dynamic-material-table',
  templateUrl: './dynamic-material-table.component.html',
  styleUrls: ['./dynamic-material-table.component.css']
})
export class DynamicMaterialTableComponent implements OnInit, AfterViewInit {
  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild('sortTable') sortTable: MatSort;
  @ViewChild('filter') filter: ElementRef;

  currentPage: PageEvent;
  currentSort: Sort;
  currentFilterData: string;
  totalCount: number;
  dataSource = new MatTableDataSource<any>();

  tableData = new TableData();

  // for adding checkbox element at first
  initialSelection = [];
  selection = new SelectionModel<Element>(true, []);

  constructor(private httpClient: HttpClient, private matPaginatorIntl: MatPaginatorIntl) {
  }

  ngOnInit() {
    this.currentPage = {
      pageIndex: 0,
      pageSize: 10,
      length: null
    };
    this.currentSort = {
      active: '',
      direction: ''
    };
    this.getIssuees();

    // 分頁切換時，重新取得資料
    this.paginator.page.subscribe((page: PageEvent) => {
      this.currentPage = page;
      this.getIssuees();
    });

    Observable.fromEvent(this.filter.nativeElement, 'keyup')
    .debounceTime(300)
    .distinctUntilChanged()
    .subscribe(() => {
      // 準備要提供給API的filter資料
      this.currentFilterData = (this.filter.nativeElement as HTMLInputElement).value;
      this.getIssuees();
    });
  }

  ngAfterViewInit() {
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  changeSort(sortInfo: Sort) {
    if (sortInfo.active === 'created_at') {
      sortInfo.active = 'created';
    }
    this.currentSort = sortInfo;
    this.getIssuees();
  }
  
  onClick() {
    let output = 'you are selecting : ';
    this.tableData.rowData.forEach(element => {
      if (this.selection.isSelected(element)) {
        output += element.position + ', ';
      }
    });
    alert(output);
  }

  getIssuees() {
    // get data from api url
    // const baseUrl = 'https://api.github.com/search/issues?q=repo:angular/material2';
    // let targetUrl = `${baseUrl}&page=${this.currentPage.pageIndex + 1}&per_page=${this.currentPage.pageSize}`;
    // if (this.currentSort.direction) {
    //   targetUrl = `${targetUrl}&sort=${this.currentSort.active}&order=${this.currentSort.direction}`;
    // }
    // if (this.currentFilterData) {
    //   targetUrl = `${targetUrl}&q=${this.currentFilterData}+in:title`;
    // }
    // this.httpClient.get<any>(targetUrl).subscribe(data => {
    //   this.totalCount = data.total_count;
    //   this.tableData = new TableData(data.items);
    //   this.dataSource.data = this.tableData.rowData;
    // });
    this.totalCount = rows.length;
    this.tableData = new TableData(rows);
    this.dataSource.data = this.tableData.rowData;
    // 從後端進行排序時，不用指定sort
    // this.dataSource.sort = this.sortTable;
    // 從後端取得資料時，就不用指定data srouce的paginator了
    // this.dataSource.paginator = this.paginator;
  }
}

const rows = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
  {position: 11, name: 'Sodium', weight: 22.9897, symbol: 'Na'},
  {position: 12, name: 'Magnesium', weight: 24.305, symbol: 'Mg'},
  {position: 13, name: 'Aluminum', weight: 26.9815, symbol: 'Al'},
  {position: 14, name: 'Silicon', weight: 28.0855, symbol: 'Si'},
  {position: 15, name: 'Phosphorus', weight: 30.9738, symbol: 'P'},
  {position: 16, name: 'Sulfur', weight: 32.065, symbol: 'S'},
  {position: 17, name: 'Chlorine', weight: 35.453, symbol: 'Cl'},
  {position: 18, name: 'Argon', weight: 39.948, symbol: 'Ar'},
  {position: 19, name: 'Potassium', weight: 39.0983, symbol: 'K'},
  {position: 20, name: 'Calcium', weight: 40.078, symbol: 'Ca'},
];

export class TableData {
  // showing data
  rowData: any[];
  hasPaginator = true;
  sortable = true;
  // for adding checkbox item in the first column
  isMultiSelect = true;

  constructor(rowData?: Object[]) {
    this.rowData = rowData;
  }

  get columnsName(): string[] {
    const columnsName = Object.keys(this.rowData[0]);
    // if (this.isMultiSelect) {
    //   columnsName.unshift('select');
    // }
    return columnsName;
  }

  get matHeaderRowDef(): string[] {
    const columnsName = Object.keys(this.rowData[0]);
    if (this.isMultiSelect) {
      columnsName.unshift('select');
    }
    return columnsName;
  }

  set activePaginator(active: boolean) {
    this.hasPaginator = active;
  }

  get activePaginator() {
    return this.hasPaginator;
  }

}
