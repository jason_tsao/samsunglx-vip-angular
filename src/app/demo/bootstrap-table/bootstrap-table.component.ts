import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bootstrap-table',
  templateUrl: './bootstrap-table.component.html',
  styleUrls: ['./bootstrap-table.component.css']
})
export class BootstrapTableComponent implements OnInit {
  invoices: Invoice[] = [
    { invoice_no: 'A', checked: false, position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
    { invoice_no: 'B', checked: false, position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
    { invoice_no: 'C', checked: false, position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
    { invoice_no: 'D', checked: false, position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
    { invoice_no: 'E', checked: false, position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
    { invoice_no: 'F', checked: false, position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
    { invoice_no: 'G', checked: false, position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
    { invoice_no: 'H', checked: false, position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
    { invoice_no: 'I', checked: false, position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
    { invoice_no: 'J', checked: false, position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
    { invoice_no: 'K', checked: false, position: 11, name: 'Sodium', weight: 22.9897, symbol: 'Na'},
    { invoice_no: 'L', checked: false, position: 12, name: 'Magnesium', weight: 24.305, symbol: 'Mg'},
    { invoice_no: 'M', checked: false, position: 13, name: 'Aluminum', weight: 26.9815, symbol: 'Al'},
    { invoice_no: 'N', checked: false, position: 14, name: 'Silicon', weight: 28.0855, symbol: 'Si'},
    { invoice_no: 'O', checked: false, position: 15, name: 'Phosphorus', weight: 30.9738, symbol: 'P'},
    { invoice_no: 'P', checked: false, position: 16, name: 'Sulfur', weight: 32.065, symbol: 'S'},
    { invoice_no: 'Q', checked: false, position: 17, name: 'Chlorine', weight: 35.453, symbol: 'Cl'},
    { invoice_no: 'R', checked: false, position: 18, name: 'Argon', weight: 39.948, symbol: 'Ar'},
    { invoice_no: 'S', checked: false, position: 19, name: 'Potassium', weight: 39.0983, symbol: 'K'},
    { invoice_no: 'T', checked: false, position: 20, name: 'Calcium', weight: 40.078, symbol: 'Ca'},
  ];

  constructor() { }

  ngOnInit() {
  }

  submit() {
    let selected = 'you have select: ';
    this.invoices.forEach(element => {
      if (element.checked) {
        selected += element.invoice_no + ', ';
      }
    });
    alert(selected);
  }

  edit(invoice_no: string) {
    alert('edit invoice_no:' + invoice_no);
  }

  view(invoice_no: string) {
    alert('view invoice_no:' + invoice_no);
  }
}

class Invoice {
  invoice_no: string;
  checked: boolean;
  name: string;
  position: number;
  weight: number;
  symbol: string;
}
