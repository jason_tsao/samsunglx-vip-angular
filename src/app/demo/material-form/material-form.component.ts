import { HttpClient } from '@angular/common/http';
import { DateUtil } from './../../util/dateUtil';
import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, FormGroupDirective, NgForm } from '@angular/forms';
// import { ErrorStateMatcher } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { MatSelect } from '@angular/material';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';

// 調整時機為invalid + dirty即顯示錯誤訊息
// export class EarlyErrorStateMatcher implements ErrorStateMatcher {
//   isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
//     const isSubmitted = form && form.submitted;
//     return !!(control && control.invalid && control.dirty);
//   }
// }

export class Model {
  public hawb: string;
  public invoice_no: string;
  public date1: Date;
  public date2: Date;
  public time1: Date;
  public time2: Date;
  public intro: string;
  public country: any;
  constructor(hawb: string, invoice_no: string, date1: Date, date2: Date, time1?: Date, time2?: Date, intro?: string, country?: any) {
    this.hawb = hawb;
    this.invoice_no = invoice_no;
    this.date1 = date1;
    this.date2 = date2;
    this.time1 = time1;
    this.time2 = time2;
    this.intro = intro;
    this.country = country;
  }

}

export class DataBaseField {
  public id: string;
  public name: string;
}

@Component({
  selector: 'app-material-form',
  templateUrl: './material-form.component.html',
  styleUrls: ['./material-form.component.css'],
  providers: [
    // { provide: ErrorStateMatcher, useClass: EarlyErrorStateMatcher },
  ]
})
export class MaterialFormComponent implements OnInit {
  options: FormGroup;
  modelForm: FormGroup;
  model: Model = new Model('TW101S220233', 'SAER23G123', new Date(), DateUtil.getDate('20171122'));
  countries$: Observable<any[]>;
  majorTechList;
  fieldsName: DataBaseField[] = [
    {'name': 'field1', 'id': '1'},
    {'name': 'field2', 'id': '2'},
    {'name': 'field3', 'id': '3'},
    {'name': 'field4', 'id': '4'},
    {'name': 'field5', 'id': '5'}
  ];
  tags = ['JavaScript', 'Material Design', 'Angular Material'];
  conditions = ['=', '>', '>=', '<', '<='];
  constructor(fb: FormBuilder, private httpClient: HttpClient) {
    this.options = fb.group({
      hideRequired: false,
      floatLabel: 'auto',
    });
  }
  ngOnInit() {
    // model driven form
    this.modelForm = new FormGroup({
      'hawb': new FormControl(this.model.hawb, [
        Validators.required,
        Validators.minLength(4),
      ]),
      'invoice_no': new FormControl(this.model.invoice_no),
      'date1': new FormControl(this.model.date1),
      'date2': new FormControl(this.model.date2),
      'time1': new FormControl(this.model.time1),
      'time2': new FormControl(this.model.time2),
      'intro': new FormControl(this.model.intro, [Validators.required, Validators.minLength(10)]),
      'country': new FormControl(this.model.country),
      'majorTech': new FormControl(this.majorTechList),
    });

    this.modelForm
    .get('country')
    .valueChanges.debounceTime(300)
    .subscribe(inputCountry => {
      this.countries$ = this.httpClient.get<any[]>('assets/countries.json').map(countries => {
        // console.log(countries);
        return countries.filter(country => country.name.indexOf(inputCountry) >= 0);
      });
    });
    this.majorTechList = [
      {
        name: '前端',
        items: ['HTML', 'CSS', 'JavaScript']
      },
      {
        name: '後端',
        items: ['C#', 'NodeJs', 'Go']
      }
    ];
  }

  submit() {
    // console.log('' + this.model.country.code);
    // console.log('' + this.modelForm.get('country').value.name);
    // console.log('hasError required==>' + this.modelForm.get('intro').hasError('required'));
    // console.log('hasError minlength==>' + this.modelForm.get('intro').hasError('minlength'));
    // console.log('Validate the From ... result =>' + this.modelForm.valid);
    // console.log('ETA:' + DateUtil.yyyymmdd(this.model.date2) + ' ' + this.model.time1);
    // console.log('HAWB=>' + this.modelForm.get('hawb').valid);
    // console.log('INVOICE=>' + this.modelForm.get('invoice_no').valid);
    // console.log('DATE1=>' + this.modelForm.get('date1').valid);
    // console.log('DATE2=>' + this.modelForm.get('date2').valid);
    // console.log('date1:' + this.model.date1.getFullYear() +
    //   '/' + this.padLeft(('' + (this.model.date1.getMonth() + 1)), '0', 2) +
    //   '/' + this.model.date1.getDate());
    this.tags.forEach(element => {
      console.log(element);
    });
  }

  highlightFiltered(countryName: string) {
    const inputCountry = this.modelForm.get('country').value;
    // console.log(countryName.replace(inputCountry, `<strong>${inputCountry}</strong>`));
    return countryName.replace(inputCountry, `<strong>${inputCountry}</strong>`);
  }

  displayCountry(country: any) {
    if (country) {
      return `${country.name} / ${country.code}`;
    } else {
      return '';
    }
  }

  removeTag(tagName) {
    this.tags = this.tags.filter(tag => tag !== tagName);
  }

  addConditions(searchField: MatSelect, searchCondition: MatSelect, searchValue: any) {
    this.tags.push(searchField.value.name + ' ' + searchCondition.value + ' ' + searchValue.value);
  }
}
