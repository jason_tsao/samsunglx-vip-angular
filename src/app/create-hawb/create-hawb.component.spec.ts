import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateHawbComponent } from './create-hawb.component';

describe('CreateHawbComponent', () => {
  let component: CreateHawbComponent;
  let fixture: ComponentFixture<CreateHawbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateHawbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateHawbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
