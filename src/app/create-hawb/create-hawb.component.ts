import { ValidatorUtil } from './../util/validatorUtil';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, FormGroupDirective, NgForm, ValidatorFn } from '@angular/forms';
import { Tracking } from '../models/tracking.model';
import { PersonInCharge } from '../models/person-in-charge.model';
import { regValidator } from '../validators/reg-validator.directive';
import { dateValidator } from '../validators/date-validator.directive';
import {
  MAT_LABEL_GLOBAL_OPTIONS,
} from '@angular/material';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-create-hawb',
  templateUrl: './create-hawb.component.html',
  styleUrls: ['./create-hawb.component.css']
})
export class CreateHawbComponent implements OnInit {
  isLinear = false;
  hawb: string;
  tracking: Tracking = new Tracking();
  incharge: PersonInCharge[];

  tpeForm: FormGroup;
  laxForm: FormGroup;
  sanForm: FormGroup;
  mexForm: FormGroup;

  basicForm: FormGroup;
  milestoneForm: FormGroup;
  additionalForm: FormGroup;

  stepIsEditable = false;

  disabled: boolean;
  tpe_disabled: boolean;
  lax_disabled: boolean;
  san_disabled: boolean;
  mex_disabled: boolean;

  obj = new Object;

  constructor(
    public route: ActivatedRoute,
    private dataservice: DataService
  ) {
    console.log('******into constructor******');
    this.hawb = this.route.snapshot.paramMap.get('hawb');
    // check the sign in user is whether editable
    this.disabled = true;
    this.tpe_disabled = false;
    this.lax_disabled = false;
    this.san_disabled = true;
    this.mex_disabled = true;

    this.incharge = [
      {id:1, name: "aaa"},
      {id:2, name: "bbb"},
      {id:3, name: "ccc"},
      {id:4, name: "ddd"},
      {id:5, name: "eee"},
    ];
  }

  ngOnInit() {
    console.log('into ngOnInit');
  // tpe a form group
    this.tpeForm = new FormGroup({
      // departure from Taiwan (export)
      'pol_etd_date': new FormControl({value: this.tracking.pol_etd_date, disabled: this.tpe_disabled}),
      'pol_etd_time': new FormControl({value: this.tracking.pol_etd_time, disabled: this.tpe_disabled}),
      'pol_atd_date': new FormControl({value: this.tracking.pol_atd_date, disabled: this.tpe_disabled}),
      'pol_atd_time': new FormControl({value: this.tracking.pol_atd_time, disabled: this.tpe_disabled}),
      // arrive LA (import)
      'pod_eta_date': new FormControl({value: this.tracking.pod_eta_date, disabled: this.tpe_disabled}),
      'pod_eta_time': new FormControl({value: this.tracking.pod_eta_time, disabled: this.tpe_disabled}),
    });
    this.laxForm = new FormGroup({
      // arrive LA (import)
      'pod_ata_date': new FormControl({value: this.tracking.pod_ata_date, disabled: this.lax_disabled}),
      'pod_ata_time': new FormControl({value: this.tracking.pod_ata_time, disabled: this.lax_disabled}),
      // pickup goods in LA (import)
      'lax_pickup_date': new FormControl({value: this.tracking.lax_pickup_date, disabled: this.lax_disabled}),
      'lax_pickup_time': new FormControl({value: this.tracking.lax_pickup_time, disabled: this.lax_disabled}),
      // deliver to LA Container Freight Station (import)
      'la_cfs_delivery_date': new FormControl({value: this.tracking.la_cfs_delivery_date, disabled: this.lax_disabled}),
      'la_cfs_delivery_time': new FormControl({value: this.tracking.la_cfs_delivery_time, disabled: this.lax_disabled}),
    });
    this.sanForm = new FormGroup({
      // deliver to SAN Container Freight Station (import)
      'sd_cfs_inbound_date': new FormControl({value: this.tracking.sd_cfs_inbound_date, disabled: this.san_disabled}),
      'sd_cfs_inbound_time': new FormControl({value: this.tracking.sd_cfs_inbound_time, disabled: this.san_disabled}),
      // departure from SAN Container Freight Station (import)
      'sd_cfs_outbound_date': new FormControl({value: this.tracking.sd_cfs_outbound_date, disabled: this.san_disabled}),
      'sd_cfs_outbound_time': new FormControl({value: this.tracking.sd_cfs_outbound_time, disabled: this.san_disabled}),
    });
    this.mexForm = new FormGroup({
      // arrive SAMEX warehouse (import)
      'samex_date': new FormControl({value: this.tracking.samex_date, disabled: this.mex_disabled}),
      'samex_time': new FormControl({value: this.tracking.samex_time, disabled: this.mex_disabled}),
    });
    // creating a form group
    this.basicForm = new FormGroup({
      'hawb': new FormControl({value: this.tracking.hawb, disabled: false}),
      'mawb': new FormControl({value: this.tracking.mawb, disabled: this.disabled}),
      'incharge': new FormControl({value: this.tracking.incharge, disabled: false}),
      'invoice': new FormControl({value: this.tracking.invoice, disabled: this.disabled}),
      'commod': new FormControl({value: this.tracking.commod, disabled: this.disabled}),
      'type': new FormControl({value: this.tracking.type, disabled: this.disabled}),
      'shipper': new FormControl({value: this.tracking.shipper, disabled: this.disabled}),
      'flight': new FormControl({value: this.tracking.flight, disabled: this.disabled}),
      'pkgs': new FormControl({value: this.tracking.pkgs, disabled: this.disabled}),
      'cartons': new FormControl({value: this.tracking.cartons, disabled: this.disabled}),
      'hot': new FormControl({value: this.tracking.hot, disabled: false}),
      'cwt': new FormControl({value: this.tracking.cwt, disabled: this.disabled}),
      'whs': new FormControl({value: this.tracking.whs, disabled: false}),
      'idr_received': new FormControl({value: this.tracking.idr_received, disabled: this.disabled}),
      'eta_samex_req_date': new FormControl({value: this.tracking.eta_samex_req_date, disabled: false}),
      'remark': new FormControl({value: this.tracking.remark, disabled: false}, [
        Validators.maxLength(500),
      ]),
    });

    // we can dynamically add validation by this way
    if (this.disabled) {
      this.basicForm.controls['remark'].setValidators(Validators.maxLength(500));
    } else {
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
  Object.keys(formGroup.controls).forEach(field => {
    const control = formGroup.get(field);
    if (control instanceof FormControl) {
      control.markAsTouched({ onlySelf: true });
    } else if (control instanceof FormGroup) {
      this.validateAllFormFields(control);
    }
  });
  }

  submit() {
    if (this.milestoneForm.valid) {
      console.log('milestoneForm submitted');
    } else {
      alert('Some fields are error!!');
      this.validateAllFormFields(this.milestoneForm);
    }
  }

  prolinkCopy() {
    alert('' + this.tracking.hawb);
    this.dataservice.getProlinkCopy(this.tracking.hawb).subscribe(response=>{
      this.tracking.setProlinkData(response.data);
    });
  }

  cancel() {
    console.log('cancel');
    window.history.back();
  }

  get eta_samex_req_date() { return this.milestoneForm.get('eta_samex_req_date'); }
}
