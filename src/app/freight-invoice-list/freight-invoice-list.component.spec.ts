import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FreightInvoiceListComponent } from './freight-invoice-list.component';

describe('FreightInvoiceListComponent', () => {
  let component: FreightInvoiceListComponent;
  let fixture: ComponentFixture<FreightInvoiceListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreightInvoiceListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreightInvoiceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
