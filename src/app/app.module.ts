import { LocalStorageHelper } from './util/localStorage';
import { AwsUtil } from './services/aws.service';
import { CognitoUtil } from './services/cognito.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule, NG_VALIDATORS, NG_ASYNC_VALIDATORS } from '@angular/forms';
import { DynamicFormsMaterialUIModule } from '@ng-dynamic-forms/ui-material';

import { BootstrapTableComponent } from './demo/bootstrap-table/bootstrap-table.component';
import { MaterialTableComponent } from './demo/material-table/material-table.component';
import { MaterialFormComponent } from './demo/material-form/material-form.component';

import { AppComponent } from './app.component';
import { MilestoneUpdateComponent } from './milestone-update/milestone-update.component';
import { MilestoneListComponent } from './milestone-list/milestone-list.component';
import { HttpClientModule } from '@angular/common/http';
import { InvoicePickupComponent } from './invoice-pickup/invoice-pickup.component';
import { CheckListReportComponent } from './check-list-report/check-list-report.component';
import { LoginConfigService } from './services/login-config.service';
import { AdalService } from './services/adal.service';
import { AuthGuard } from './guard/auth.guard';
import { LoginDialogComponent } from './login-dialog/login-dialog.component';
import { UserLoginService } from './services/cognito-login.service';
import { UserRegistrationService } from './services/user-registration.service';
import { DataService } from './services/data.service';

import { LoginComponent } from './auth/login/login.component';
import { LogoutComponent } from './auth/logout/logout.component';
import { RegisterComponent } from './auth/register/register.component';
import { ForgotPasswordComponent } from './auth/forgot-password/forgot-password.component';
import { ResendComponent } from './auth/resend/resend.component';
import { NewpasswordComponent } from './auth/newpassword/newpassword.component';
import { ConfirmComponent } from './auth/confirm/confirm.component';
import { ForgotPasswordStep2Component } from './auth/forgot-password-step2/forgot-password-step2.component';
import { HomepageComponent } from './homepage/homepage.component';
import { AuthHomeComponent } from './auth/auth-home/auth-home.component';
import { InvoicePickupDialogComponent } from './invoice-pickup-dialog/invoice-pickup-dialog.component';
import { TestComponent } from './demo/test/test.component';
import { UiMaterialComponent } from './demo/ui-material/ui-material.component';
import { DynamicFormService, DynamicFormValidationService, DynamicFormLayoutService } from '@ng-dynamic-forms/core';
import { DynamicFormsCoreModule, DYNAMIC_VALIDATORS, Validator, ValidatorFactory } from '@ng-dynamic-forms/core';
import { ValidationMessageComponent } from './validation-message/validation-message.component';
import {
  customValidator,
  customDateRangeValidator,
  customForbiddenValidator,
  customAsyncFormGroupValidator
} from './app.validators';
import { DynamicMaterialTableComponent } from './demo/dynamic-material-table/dynamic-material-table.component';
import { CreateHawbComponent } from './create-hawb/create-hawb.component';
import { FreightInvoiceListComponent } from './freight-invoice-list/freight-invoice-list.component';
import { FreightInvoiceComponent } from './freight-invoice/freight-invoice.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  { path: '', redirectTo: 'secure', pathMatch: 'full' },
  { path: 'login', component: AuthHomeComponent,
    children: [
      { path: 'register', component: RegisterComponent },
      { path: 'confirm', component: ConfirmComponent },
      { path: 'confirm/:username', component: ConfirmComponent },
      { path: 'resendCode', component: ResendComponent },
      { path: 'resendCode/:username', component: ResendComponent },
      { path: 'forgotPassword', component: ForgotPasswordComponent },
      { path: 'forgotPassword/:username', component: ForgotPasswordStep2Component },
      { path: 'newPassword', component: NewpasswordComponent },
      { path: '', component: LoginComponent },
    ]
  },
  { path: 'logout', component: LogoutComponent },
  { path: 'secure', component: HomepageComponent, canActivate: [AuthGuard],
    children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'invoicePickup', component: InvoicePickupComponent },
      { path: 'createHawb', component: CreateHawbComponent },
      { path: 'milestoneList', component: MilestoneListComponent },
      { path: 'milestoneUpdate/:hawb', component: MilestoneUpdateComponent },
      { path: 'freightInvoiceList', component: FreightInvoiceListComponent },
      { path: 'checkListReport', component: CheckListReportComponent },
      // for demo
      { path: 'materialTable', component: MaterialTableComponent },
      { path: 'dynamic-MaterialTable', component: DynamicMaterialTableComponent },
      { path: 'materialForm', component: MaterialFormComponent },
      { path: 'bootstrapTable', component: BootstrapTableComponent },
      { path: 'ui-Material', component: UiMaterialComponent },
      { path: 'test', component: TestComponent },
    ]
  },
  // otherwise redirect to home
  { path: '**', redirectTo: 'secure' },
];

@NgModule({
  declarations: [
    AppComponent,
    BootstrapTableComponent,
    MilestoneUpdateComponent,
    MilestoneListComponent,
    MaterialTableComponent,
    MaterialFormComponent,
    InvoicePickupComponent,
    CheckListReportComponent,
    // Login feature
    LoginComponent,
    LogoutComponent,
    LoginDialogComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    ResendComponent,
    NewpasswordComponent,
    ConfirmComponent,
    ForgotPasswordStep2Component,
    HomepageComponent,
    AuthHomeComponent,
    InvoicePickupDialogComponent,
    TestComponent,
    // Dynamic Material Example
    UiMaterialComponent,
    ValidationMessageComponent,
    DynamicMaterialTableComponent,
    CreateHawbComponent,
    FreightInvoiceListComponent,
    FreightInvoiceComponent,
    DashboardComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    DynamicFormsMaterialUIModule,
    DynamicFormsCoreModule,
  ],
  providers: [
    LoginConfigService,
    AdalService,
    AuthGuard,
    UserLoginService,
    CognitoUtil,
    AwsUtil,
    LocalStorageHelper,
    UserRegistrationService,
    DynamicFormService,
    DynamicFormValidationService,
    DynamicFormLayoutService,
    DataService,
    {
        provide: NG_VALIDATORS,
        useValue: customValidator,
        multi: true
    },
    {
        provide: NG_VALIDATORS,
        useValue: customDateRangeValidator,
        multi: true
    },
    {
        provide: NG_ASYNC_VALIDATORS,
        useValue: customAsyncFormGroupValidator,
        multi: true
    },
    {
        provide: DYNAMIC_VALIDATORS,
        useValue: new Map<string, Validator | ValidatorFactory>([
            ['customValidator', customValidator],
            ['customDateRangeValidator', customDateRangeValidator],
            ['customForbiddenValidator', customForbiddenValidator],
            ['customAsyncFormGroupValidator', customAsyncFormGroupValidator]
        ])
    }
  ],
  entryComponents: [
    LoginDialogComponent,
    InvoicePickupDialogComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
