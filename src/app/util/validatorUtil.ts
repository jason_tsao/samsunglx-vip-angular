import { Validators } from '@angular/forms';
export class ValidatorUtil {
    static getPattern(inputFormat: string) {
        switch (inputFormat) {
            case 'yyyyMMdd':
                return '^[12][0-9]{3}(0[1-9]|1[0-2])(0[1-9]|[12][0-9]|3[01])$';
            case 'hhmm':
                return '^([0-1][0-9]|[2][0-3])([0-5][0-9])$';
            case 'MM/dd/yyyy':
                return '^([1-9]|1[0-2])\/(0[1-9]|[12][0-9]|3[01])\/[12][0-9]{3}$';
            case 'hhmm':
                return '^([0-1][0-9]|[2][0-3])([0-5][0-9])$';
            default:
                throw new Error('Your format is not exist in code now.');
        }
    }
    // Regular expression yyyymmdd
    static getYyyymmddPattern() {
        return '^[12][0-9]{3}(0[1-9]|1[0-2])(0[1-9]|[12][0-9]|3[01])$';
    }
    static getHhmmPattern() {
        return '^([0-1][0-9]|[2][0-3])([0-5][0-9])$';
    }
    // Regular expression mm/dd/yyyy
    static getMmddyyyyPattern() {
        return '^([1-9]|1[0-2])\/(0[1-9]|[12][0-9]|3[01])\/[12][0-9]{3}$';
    }
    static getValidatorYyyymmdd() {
        return Validators.pattern(new RegExp('^([12][0-9]{3}(0[1-9]|1[0-2])(0[1-9]|[12][0-9]|3[01]))$'));
    }
    // Regular expression hhmm
    static getValidatorHhmm() {
        return Validators.pattern(new RegExp('^([0-1][0-9]|[2][0-3])([0-5][0-9])$'));
    }
}
