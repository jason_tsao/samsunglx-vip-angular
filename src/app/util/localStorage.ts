export class LocalStorageHelper {
    public localStorageItem(id: string): string {
        return localStorage.getItem(id);
    }
}
