export class DateUtil {
    /**
     * input Date object => return string yyyymmdd
     * @param date
     */
    static getPattern (date: Date, pattern: string) {
        if (date && date instanceof Date) {
            switch (pattern.toLocaleLowerCase()) {
            case 'yyyymmdd':
                const mm = date.getMonth() + 1; // getMonth() is zero-based
                const dd = date.getDate();
                return [
                    date.getFullYear(),
                    (mm > 9 ? '' : '0') + mm,
                    (dd > 9 ? '' : '0') + dd
                ].join('');
            default:
                throw new Error('pattern is not support yet');
            }
        }
    }
    // string yyyyMMdd => return javascript Date object
    /**
     * string yyyyMMdd => return javascript Date object
     * @param dateString
     */
    static getDate(dateString: string) {
        const year = Number(dateString.substring(0, 4));
        const month = Number(dateString.substring(4, 6));
        const day = Number(dateString.substring(6, 8));
        return new Date(year, month - 1, day);
    }
}
