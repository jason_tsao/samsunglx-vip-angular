import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable()
export class LoginConfigService {

  constructor() { }

  public get getAdalConfig(): any {
    return environment.adal;
  }

  public get getCognitoConfig(): any {
    return environment.cognito;
  }
}
