import { Injectable } from '@angular/core';

/**
 * base class for service
 */
@Injectable()
export class HttpBaseService {
  constructor() {
  }
  public handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}

