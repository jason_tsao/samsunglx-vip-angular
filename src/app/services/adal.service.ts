import { LoginConfigService } from './login-config.service';
import { Injectable } from '@angular/core';
// import 'expose-loader?AuthenticationContext!../../../node_modules/adal-angular/lib/adal.js';
declare var adal; // angular-adal/lib/adal.js
import * as _ from 'adal'; // @types/adal

@Injectable()
export class AdalService {
  createAuthContextFn: adal.AuthenticationContextStatic = AuthenticationContext;
  private context: adal.AuthenticationContext;

  constructor(private loginConfigService: LoginConfigService) {
      this.context = new this.createAuthContextFn(loginConfigService.getAdalConfig);
  }

  login() {
      this.context.login();
  }

  logout() {
      this.context.logOut();
  }

  handleCallback() {
      this.context.handleWindowCallback();
  }

  // TODO: test for refresh token
  refreshToken() {
      this.context.acquireToken(
          this.loginConfigService.getAdalConfig.clientId,
          function(message: string, token: string) {
            if (token) {
            } else {
                throw new Error(message);
            }
          }
      );
  }

  public get userInfo() {
      return this.context.getCachedUser();
  }

  public get accessToken() {
      return this.context.getCachedToken(this.loginConfigService.getAdalConfig.clientId);
  }

  public get isAuthenticated() {
      return this.userInfo && this.accessToken;
  }
}
