import { HttpBaseService } from './http-base.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

/**
 * VIP tracking data service API
 */
@Injectable()
export class TrackingService extends HttpBaseService{

  constructor(private http: HttpClient) {
    super();
  }

  /**
   * save inbound scan record
   * @param inboundScanRecord
   */
  // saveInboundScanInfo(inboundScanRecord: InboundScanRecord): Observable<ApiResponse> {
  //   return this.http.post<ApiResponse>(
  //     environment.kivieDataServiceUrl + `/saveInboundScanInfo`, JSON.stringify(inboundScanRecord));
  // }

  /**
   * get scan count by pallet id
   * @param inboundScanRecord
   */
  // getScanCount(asnNumber: string, palletId: string, productId: string): Observable<number> {
  //   return this.http.get<number>(environment.kivieDataServiceUrl +
  //    `/getScanCount?asnNumber=${asnNumber}&palletId=${palletId}&productId=${productId}`);
  // }
}
