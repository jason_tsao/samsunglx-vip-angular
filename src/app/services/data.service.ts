import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { HttpBaseService } from './http-base.service';
import { Observable } from 'rxjs/Observable';
import { Invoice } from '../models/Invoice.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': environment.dataToken
  })
}

@Injectable()
export class DataService extends HttpBaseService {

  constructor(
    private httpClient: HttpClient
  ) {
    super();
  }

  getInvoicList(from_date, to_date, page_index, record_per_page): Observable<any> {
    return this.httpClient.get<any>(environment.dataServiceUrl + 'invoicList?date=' + from_date, httpOptions);
  }

  postUpdateInvoic(hawb, invoice_no_list): Observable<any> {
    return this.httpClient.post<any>(environment.dataServiceUrl + 'updateInvoic', {hawb, invoice_no_list}, httpOptions);
  }

  getTrackingList(from_date, to_date, page_index, record_per_page): Observable<any> {
    var myurl = 'http://www.mocky.io/v2/5ba1f6db2f000074008d2b03';
    // return this.httpClient.get<any>(environment.dataServiceUrl + 'invoicList?date=' + from_date, httpOptions);
    return this.httpClient.get<any>(myurl);
  }

  getProlinkCopy(hawb: string): Observable<any> {
    var myurl = 'http://www.mocky.io/v2/5ba1f6db2f000074008d2b03';
    // return this.httpClient.get<any>(environment.dataServiceUrl + 'hawb=' + hawb, httpOptions);
    return this.httpClient.get<any>(myurl);
  }
}