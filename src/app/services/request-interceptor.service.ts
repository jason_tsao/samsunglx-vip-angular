import { UserLoginService } from './cognito-login.service';
import { Injectable } from '@angular/core';
// TODO: remove not used import...
import { HttpRequest, HttpHandler, HttpEvent, HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { HttpSentEvent, HttpHeaderResponse, HttpProgressEvent, HttpUserEvent, HttpErrorResponse } from '@angular/common/http';
import { HttpResponse } from '@angular/common/http';
import { HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

/**
 * Intercepts `HttpRequest` and handles them.
 *
 * We add jwt token to header here, and also refresh token(cognito only).
 *
 * Reference:
 *   https://www.intertech.com/Blog/angular-4-tutorial-handling-refresh-token-with-new-httpinterceptor/
 *
 * ADAL renew token don't need to implement (angular-adal done this for us):
 *   https://stackoverflow.com/questions/37535366/acquiring-new-access-token-using-refresh-token-adal-js
 *   http://www.cloudidentity.com/blog/2015/02/19/introducing-adal-js-v1/  <== Renewing Tokens Chapter
 *
 * Cognito refresh token, call CognitoUser.getSession() :
 *   https://stackoverflow.com/questions/37442973/cognito-user-pool-how-to-refresh-access-token-using-refresh-token
 */
@Injectable()
export class RequestInterceptorService implements HttpInterceptor {
  isRefreshingToken = false;
  tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  constructor(private userService: UserLoginService) { }

  // add bearer token to header.
  addTokenToHeader(req: HttpRequest<any>, token: string): HttpRequest<any> {
    return req.clone({ setHeaders: { Authorization: 'Bearer ' + token }});
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.userService.isAuthenticated(this); // this will auto refresh cognito token

    return next.handle(this.addTokenToHeader(req, 'add token here'));
      // .catch(error => {
      //     if (error instanceof HttpErrorResponse) {
      //       switch ((<HttpErrorResponse>error).status) {
      //         case 400: // handle bad request error
      //           return this.handle400Error(error);
      //         case 401: // handle unauthorized error
      //           return this.handle401Error(req, next);
      //         default:
      //           return Observable.throw(error);
      //       }
      //     } else { // success
      //       // return Observable.throw(error);
      //     }
      // });
  }

  handle400Error(error) {
    if (error && error.status === 400 && error.error && error.error.error === 'invalid_grant') {
        // If we get a 400 and the error message is 'invalid_grant', the token is no longer valid so logout.
        return this.logoutUser();
    }
    return Observable.throw(error);
  }

  logoutUser() {
    // Route to the login page (implementation up to you)
    return Observable.throw('');
  }

  handle401Error(req: HttpRequest<any>, next: HttpHandler) {
    // TODO: implement handling error
    return Observable.throw('');
  }

    // cognito isAuthenticated()'s callback function
    isLoggedIn(message: string, isLoggedIn: boolean) {
      if (isLoggedIn) {
        console.log('User is login through cognito!!');
      } else {
        console.log('User is not login through cognito!!');
      }
    }
}

