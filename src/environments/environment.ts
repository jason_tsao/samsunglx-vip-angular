// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  env: 'DEV',
  production: false,
  dataServiceUrl: 'http://127.0.0.1:8080/api/1.0/samsung/',
  dataToken: 'Basic cmVzdEFQSTpyZXN0QVBJ',
  adal: {
    tenant: 'morrisonexpress.com',
    clientId: '1ea95d65-0144-4a1d-9270-58166b600dd4',
    redirectUri: window.location.origin + '/',
    postLogoutRedirectUri: window.location.origin + '/'
  },
  cognito: {
    region: 'us-west-2',
    identityPoolId: '',
    userPoolId: 'us-west-2_Sga1CKCBS',
    clientId: '7l01j3pak3qfla00f42df8sg30',
    rekognitionBucket: 'rekognition-pics',
    albumName: 'usercontent',
    bucketRegion: 'us-west-2',
    ddbTableName: 'LoginTrail',
    cognito_idp_endpoint: '',
    cognito_identity_endpoint: '',
    sts_endpoint: '',
  }
};
